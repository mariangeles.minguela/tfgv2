﻿using UnityEngine;
using System.Net.Mail;
using FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples;
using System.Collections;

public class MailManager : MonoBehaviour
{

    private LoginManager loginManager;
    private string teacherMail = "mariangeles.minguela@gm.uca.es";

    void Start()
    {
        loginManager = FindObjectOfType<LoginManager>();
    }

    // Use this for initialization
    public void SendMail()
    {
        string filePath = Application.persistentDataPath + "/" + loginManager.GetUsername() + ".csv";
        MailMessage mail = new MailMessage();
        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
        mail.From = new MailAddress("iswappbot@gmail.com");
        mail.To.Add(loginManager.GetMail());
        mail.To.Add(teacherMail);

        if(loginManager.GetUsername() != null)
        {
            mail.Subject = "Resultados pruebas " + loginManager.GetUsername();
        }else
        {
            mail.Subject = "Reporte pruebas";
        }
        
        mail.Body = "Hola\n Adjunto el fichero CSV con los datos recabados del usuario durante su desarrollo del entrenamiento, \n\n Gracias por su colaboración \n Un saludo.";

        
        System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(filePath);
        mail.Attachments.Add(attachment);

        SmtpServer.Port = 587;
        SmtpServer.Credentials = new System.Net.NetworkCredential("iswappbot@gmail.com", "iswapp3!");
        SmtpServer.EnableSsl = true;

        SmtpServer.Send(mail);

        //para que funcione con una cuenta de gmail hay que entrar aqui:
        //https://myaccount.google.com/lesssecureapps?pli=1
    }
}