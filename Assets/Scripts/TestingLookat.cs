﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingLookat : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
    }
    void OnEnable()
    {
        if (gameObject.name.Equals("Activities"))
        {
            transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2f;
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1);
        }
        else if (gameObject.tag.Equals("Question"))
        {
            transform.position = Camera.main.transform.position + Camera.main.transform.forward * 3;
        }
        else if (gameObject.tag.Equals("EndInterview"))
        {
            transform.position = Camera.main.transform.position + Camera.main.transform.forward * 3;
        }else
        {
            transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2;
        }
        transform.position = new Vector3(transform.position.x, transform.position.y + 0.7f, transform.position.z);
        transform.rotation = Camera.main.transform.rotation;

    }

}
