﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Keywords
{
    public static string[] correctWordsSaludo = { "buenas", "tardes", "buenos", "días", "trabajador", "trabajadora", "social", "centro", "servicios", 
                                    "sociales", "entrevista", "visita", "domiciliaria", "valoracion", "valoración", "grado", "situación", 
                                    "situacion", "dependencia"};
    public static string[] incorrectWordsSaludo = { "hola", "saber", "cuestionario"};
    
    public static string[] correctWordsComerBeber = { "podría", "podrías", "podria", "podrias", "reconoces", "reconoce", "alimentos", "alimento", "comes", "come", "servidos", 
                                                        "identifica", "identificas", "dificultad", "alcanzar", "necesita", "ayuda", "servirte", "comida", "utiliza", 
                                                        "cuchillo", "triturada", "necesitas", "cortar", "partir", "pequeño", "cubiertos", "trocear", "trozos", "trozo",
                                                        "difícil", "llevarte", "llevarse", "llevar", "mismo", "otra", "persona", "acercarse", "recipiente", "vaso", 
                                                        "liquido", "líquido","cocina", "coger", "agarrar", "comprobar", "comprobación" };

    public static string[] incorrectWordsComerBeber = { "capaz", "recuerda", "caer", "ingerir", "consciente"};

    public static string[] correctWordsMiccionDefecacion = {"solo", "váter", "dificultad", "apoyos", "necesidades", "uso", "orinar", "lugar", "adecuado", "defecar", "limpiarse", "adecuada",
                                                            "limpio", "limpia", "bien", "mal", "limpieza", "identificar","ropa", "estación" ,"año", "sucia" , "reconocer", "lavar",
                                                            "ayuda", "otra", "persona", "postura", "posición", "mucho", "tiempo", "mismo", "correcta", "buena", "dolor", "molestias",
                                                            "cambiar", "baño" , "olor" ,"sucio" ,"difícil" ,"apoyo", "productos", "intima" , "personal", "intimo", "pañal", "comprobar", 
                                                            "comprobación"};

    public static string[] incorrectWordsMiccionDefecacion = { "culo", "ojete" };

    public static string[] correctWordsLavarse = {"abrir", "cerrar", "problemas", "frío", "caliente", "grifos", "todo", "bañarse", "partes", "cuerpo", "agua", "toalla", 
                                                  "toallas", "pies", "cara", "pelo", "secarse", "toalla", "parte", "inferior", "superior", "aseo", "agarraderas", "asideros",
                                                  "apoyo", "alfombrilla", "taburete", "silla", "espalda", "glúteos", "manos", "productos", "jabón", "dificultad", "sucias",
                                                  "personas", "veces", "lavar", "bañera", "ducha", "entrar", "salir", "apoyada", "lavarte", "lavarse", "zona", "intima", 
                                                  "intimas", "difícil", "dificultades", "apoyos", "ayuda", "ayudas", "otra", "persona", "solo", "sola", "autónoma", "higiene",
                                                  "sentado", "sentada", "apoyado", "pene", "vagina", "comprobar", "comprobación"};

    public static string[] correctWordsCuidadosCorporales = {"cepillarse", "brazos", "peinarse", "peinarte", "ducha", "autónoma", "salón", "uña", "tijeras", "cortador",
                                                             "autónomo", "apoyado", "veces", "pelo", "higiene", "champú", "apoyos", "solo", "pie", "apoyada", "lavabo",
                                                             "semana", "mes", "secado", "dientes", "pasta", "boca", "difícil", "dificultades", "ayuda", "sola", "sentado", 
                                                             "sentada", "baño", "días", "frecuencia", "comprobar", "comprobación"};

    public static string[] correctWordsVestirse = {"cómoda", "coger", "calzado", "reconoces", "combinas", "combina", "combinar", "solo", "frecuencia", "invierno", "verano", "otoño", 
                                                   "primavera", "persona", "vestirse", "vestir", "armario", "armarios", "alcanzar", "zapatos", "zapatillas", "abrir","cerrar","clip",
                                                   "correcto", "calzador", "autónomo", "temporada", "arriba", "fácil", "tipos", "tipo", "ojal", "camisa", "cómodo", "cambios",
                                                   "cremallera", "movilidad", "autónoma", "sentada", "pie", "anchas", "calcetines", "pantalones", "pijama", "falda",
                                                   "medias","pantis", "ropa", "interior", "bragas", "calzoncillos", "cinturón", "chaqueta", "mover", "brazos", "botones",
                                                   "prendas", "velcro", "sola", "sentado", "difícil", "otra", "ayuda", "sujetador", "camiseta", "siente", "sientes", "sentirse", 
                                                   "comprobar", "comprobación"};
    
    public static string[] correctWordsMantenimientoSalud = {"teléfono", "emergencias", "cita", "médico", "farmacia", "enfermera", "urgencias", "número", "dificultades", 
                                                            "medicamentos", "pastillas", "toma", "cuantas", "indicaciones", "otra", "pastillero", "alarmas", "consejo", 
                                                            "tratamiento", "recetas", "fuego", "detecta", "segura", "deslizante", "gas", "cerrado", "preocupada", "solo", 
                                                            "baño", "cocina", "aceite", "escalones", "butano", "evita", "peligro", "riesgo", "fuera", "domicilio", "casa", 
                                                            "piso", "escaleras", "aceras", "bastón", "andador", "sola", "ayuda", "persona", "silladeruedas", "vecino", 
                                                            "obstáculos", "cambia", "camino", "itinerario", "llamar", "061", "112", "botón", "teleasistencia", "hijo", 
                                                            "hija", "vecina", "familiar", "comprobar", "comprobación"};

    public static string[] correctWordsPosicionCuerpo = {"tumbarse", "mantenerse", "acostarse", "dificultad", "adaptada", "varales", "tumbada", "sentada", "articulada",
                                                         "levantarse", "incorporarse", "espalda", "cómodo", "articulado", "reposapiés", "dolor", "postura", "adaptado",
                                                         "sofá", "bastón", "familiar", "sola", "solo", "mismo", "cansancio", "movilidad", "tiempo", "ayuda", "andador",
                                                         "sillón", "piernas", "brazos", "hijo", "mesa", "utiliza", "sujetas", "temblor", "trabajo", "cuerpo", "rodillas",
                                                         "apoya", "apoyo", "cambiar", "cama", "silla", "familia", "apoyos", "autonomía", "autónoma", "moverse", "equilibrio",
                                                         "desequilibrio", "difícil", "hija", "temblores", "comprobar", "comprobación"};

    public static string[] correctWordsDentroHogar = {"caminar", "vestirse", "armario", "cajones", "difícil", "dificultad", "ayuda", "utiliza", "bastón", "autónoma", "dolor", 
                                              "mismo", "temblores", "salón", "cuarto", "comer", "desplazamientos", "hijo", "hija", "tiempo", "lavarse", "cocina", "apoyo",
                                              "cansancio", "habitación", "baño", "sala", "teléfono", "leer", "televisión", "familia", "andador", "estar", "movilidad", 
                                              "silla", "ruedas", "piernas", "vecino", "vecina", "misma", "terraza", "pasillo", "balcón", "garaje", "jardín", "recibidor", 
                                              "comprobar", "comprobación"};

    public static string[] correctWordsFueraHogar = {"caminar", "pasear", "cerca", "lejos", "vivienda", "aquí", "medio", "trasporte", "silla", "desplazamientos", "vecinos",
                                                    "solo", "vecina", "vecino", "familiar", "escaleras", "portal", "pasillos", "conocidos", "sitios", "lugares", "seguro",
                                                    "entorno", "miedo", "dificultad", "movilidad", "ayuda", "familia", "utiliza", "apoyo", "apoyos", "bastón", "andador",
                                                    "tiempo", "cansancio", "autónoma", "dolor", "mismo", "misma", "temblores", "ruedas", "piernas", "salón", "terraza", 
                                                    "pasillo", "balcón", "garaje", "jardín", "coche", "autobús", "comprobar", "comprobación"};

    public static string[] correctWordsTareasDomesticas = { "prepara", "comida", "hace", "compra", "limpiar", "vivienda", "lavar", "planchar", "tender", "lavadora", 
                                                            "secadora", "polvo", "suelo", "cocina", "servir", "comidas", "frías", "preparadas", "calientes", "dieta", 
                                                            "saludable", "sana", "bebidas", "ordenar", "quitar", "barrer", "ventanas", "mesa", "cazuelas", "menaje", 
                                                            "cubiertos", "baño", "wáter", "vater", "váter", "inodoro", "lavabo", "paredes", "armario", "limpia", "friega", 
                                                            "ordena", "tiende", "cuenta", "tickets", "revisa", "dinero", "cambiar", "lista", "limpieza", "ropa"
                                                            , "comprobar", "comprobación"};

    public static string[] correctWordsDespedida = {"gracias", "muchas", "mucha", "gracia", "muchísima", "baremo", "resultados", "resultado", "meses", "mes",
                                                    "mejoría", "mejorará", "servicios", "servicio", "dependencia", "ánimo", "adiós", "buenos", "bueno", "día", "días"};
}
