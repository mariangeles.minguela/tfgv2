﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginManager : MonoBehaviour
{
    public GameObject username;
    public GameObject mail;
    private string userName;
    private string email;
    private bool emailValid = false;

    private SceneManager sceneManager;

    /// <summary>
    /// Regular expression, which is used to validate an E-Mail address.
    /// </summary>
    public const string matchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
          + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";


    public string GetUsername()
    {
        return userName;
    }

    public string GetMail()
    {
        return email;
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        sceneManager = FindObjectOfType<SceneManager>();
        //CSVManager.CreateReport();
    }

    /// <summary>
    /// Checks whether the given Email-Parameter is a valid E-Mail address.
    /// </summary>
    /// <param name="email">Parameter-string that contains an E-Mail address.</param>
    /// <returns>True, wenn Parameter-string is not null and contains a valid E-Mail address;
    /// otherwise false.</returns>
    public void CheckMail()
    {

        userName = username.GetComponent<InputField>().text;
        email = mail.GetComponent<InputField>().text;

        if (email != null)
        {
            if (Regex.IsMatch(email, matchEmailPattern))
            {
                StartCoroutine(sceneManager.LoadSceneAsync("Tutorial"));
            }
            else
            {
                SSTools.ShowMessage("Mail incorrecto", SSTools.Position.bottom, SSTools.Time.twoSecond);
            }
        }
    }

    public void CheckUcaMail()
    {
        string[] datosUsuario = new string[8];
        datosUsuario[0] = username.GetComponent<InputField>().text;
        datosUsuario[1] = mail.GetComponent<InputField>().text;

        for (int i = 2; i < datosUsuario.Length; i++)
        {
            datosUsuario[i] = "";
        }

        email = mail.GetComponent<InputField>().text;
        userName = username.GetComponent<InputField>().text;
        if (email.Contains("@alum.uca.es"))
        {
            if (!userName.Equals(""))
            {
                CSVManager.CreateReport(userName);
                //Empezamos poniendo nombre del alumno en el CSV
                CSVManager.AppendToReport(datosUsuario);
                StartCoroutine(sceneManager.LoadSceneAsync("Tutorial"));
            }else
            {
                SSTools.ShowMessage("Nombre incorrecto", SSTools.Position.bottom, SSTools.Time.twoSecond);
            }
        }
        else
        {
            SSTools.ShowMessage("Mail incorrecto", SSTools.Position.bottom, SSTools.Time.twoSecond);
        }
    }
}
