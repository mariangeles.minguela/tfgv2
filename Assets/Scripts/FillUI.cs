﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FillUI : MonoBehaviour
{
    public Image image;
    public Sprite sprite;
    public Sprite spriteInicial;

    public GameObject nextQuestion, currentQuestion, activityGO, itemsRelacionados;
    private bool fill;

    public Text answerSelected;

    public int id;

    private TrainingManager trainingManager;

    void Start()
    {
        trainingManager = FindObjectOfType<TrainingManager>();
    }
    public void FillOnPointerEnter()
    {
        fill = true;
        StartCoroutine(FillAmount());

    }

    public void UnfillOnPointerExit()
    {
        fill = false;
        StartCoroutine(UnfillAmount(30f));
    }

    IEnumerator UnfillAmount(float time)
    {
        float animationTime = time;
        while (animationTime > 0 && !fill)
        {
            animationTime -= Time.deltaTime;
            image.fillAmount -= Mathf.Lerp(image.fillAmount, 0, animationTime / time);
            //image.fillAmount -= animationTime / time;
            yield return null;
        }
    }


    public IEnumerator FillAmount()
    {
        bool once = true;
        float i = 0.0f;
        float time = 200f;
        while (i < 1.0 && fill)
        {
            i += Time.deltaTime / time;
            image.fillAmount += Mathf.Lerp(0, 1, i);
            if (image.fillAmount >= 0.999 && once)
            {
                image.sprite = sprite;
                image.type = Image.Type.Simple;
                once = false;
                yield return new WaitForSeconds(0.5f);

                switch (gameObject.tag)
                {
                    case "Question":
                        //le paso al training manager la respuesta escogida por el usuario
                        trainingManager.AnswerQuestions(answerSelected.text, id);
                        NextQuestion();
                        break;

                    case "Activity":
                        //notificamos al training manager la actividad escogida por el usuario
                        activityGO.SetActive(false);
                        itemsRelacionados.GetComponent<ShowItemsRelated>().SetActivitySelected(answerSelected.text);
                        itemsRelacionados.SetActive(true);
                        break;

                    case "EndInterview":
                        trainingManager.EndInterview();
                        break;

                    case "DemasiadosErrores":
                        //Mandamos el correo y cerramos la app.
                        trainingManager.creditos.SetActive(true);
                        break;

                    case "EndExperience":
                        //Mandamos el correo y cerramos la app.

                        Application.Quit();
                        break;
                }

                //Compruebo si el usuario está marcando una pregunta o una actividad
                /*if (gameObject.tag.Equals("Question"))
                {
                    //le paso al training manager la respuesta escogida por el usuario
                    trainingManager.AnswerQuestions(answerSelected.text, id);
                    NextQuestion();
                }else if (gameObject.tag.Equals("Activity"))
                {
                    //notificamos al training manager la actividad escogida por el usuario
                    activityGO.SetActive(false);
                    Debug.Log("Notificamos al TM ----> " + answerSelected.text);
                    trainingManager.ExecuteActivity(answerSelected.text);
                }else if(gameObject.tag.Equals("EndInterview"))
                {
                    trainingManager.EndInterview();
                }*/

            }
            yield return null;
        }
    }

    public void NextQuestion()
    {
        currentQuestion.SetActive(false);
        ResetItems();
        if (nextQuestion != null)
        {
            nextQuestion.SetActive(true);
        }
    }

    public void ResetItems()
    {
        image.sprite = spriteInicial;
        image.type = Image.Type.Filled;
        image.fillAmount = 0;
    }


    #region Debug
    [ContextMenu("Probando")]
    void Debug00()
    {
        ResetItems();
    }
    #endregion
}