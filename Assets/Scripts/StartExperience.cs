﻿using System;
using System.Collections;
using System.Collections.Generic;
using FrostweepGames.Plugins.GoogleCloud.SpeechRecognition;
using FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples;
using UnityEngine;
using UnityEngine.UI;

public class StartExperience : MonoBehaviour
{
    public Image image;
    public Sprite sprite;

    public GameObject button, demasiadosErrores;
    private Step[] steps;

    private int numberOfErrorVideoPlayed = 0;

    private Custom_GCSR_Example customGCSR;

    private VideoManager videoManager;
    private TrainingManager trainingManager;

    public bool etapaBienvenidaCompleted = false;

    private bool fill;

    void Start()
    {

        customGCSR = FindObjectOfType<Custom_GCSR_Example>();
        videoManager = FindObjectOfType<VideoManager>();
        trainingManager = FindObjectOfType<TrainingManager>();
        steps = new Step[1];
        steps[0] = new Step("Video4_7_10", "Video5", "Video6_Entrada", Keywords.correctWordsSaludo, Keywords.incorrectWordsSaludo, 5, 1);
    }
    public void FillOnPointerEnter()
    {
        fill = true;
        StartCoroutine(FillAmount());
    }

    public void UnfillOnPointerExit()
    {
        fill = false;
        StartCoroutine(UnfillAmount(30f));
    }

    public void ExecuteStep()
    {
        videoManager.PlayVideo(steps[0].GetAssociatedGeneralVideo(), true, false, false);
    }

    public void ExecuteVoiceReconigtion()
    {
        customGCSR.StartRecordButtonOnClickHandler();
    }
    IEnumerator UnfillAmount(float time)
    {
        float animationTime = time;
        while (animationTime > 0 && !fill)
        {
            animationTime -= Time.deltaTime;
            image.fillAmount -= Mathf.Lerp(image.fillAmount, 0, animationTime / time);
            //image.fillAmount -= animationTime / time;
            yield return null;
        }
    }


    public IEnumerator FillAmount()
    {
        bool once = true;
        float i = 0.0f;
        float time = 200f;
        while (i < 1.0 && fill)
        {
            i += Time.deltaTime / time;
            image.fillAmount += Mathf.Lerp(0, 1, i);
            if (image.fillAmount >= 0.999 && once)
            {
                image.sprite = sprite;
                image.type = Image.Type.Simple;
                once = false;
                yield return new WaitForSeconds(0.5f);
                button.SetActive(false);
                videoManager.PlayVideo("Video1", false, false, false);
            }
            yield return null;
        }
    }
    
    //Este metodo recoge lo que el usuario ha dicho y lo compara
    //con las palabras correctas asociadas al paso actual.
    //Si el usuario tiene más de X aciertos entonces continuará al siguiente paso.
    public void ProcessTranscript(WordInfo[] transcript)
    {
        int numberKeywordsSaidCorrectly = 0;
        int numberKeywordsSaidIncorrectly = 0;
        Step stepSelected = steps[0];
        List<string> clearTranscript = new List<string>();

        foreach (WordInfo item in transcript)
        {
            //Primero procesamos la palabra para quitarle cualquier caracter extraño
            if (item.word.Contains("."))
            {
                item.word = item.word.Replace(".", "");
            }
            if (item.word.Contains(","))
            {
                item.word = item.word.Replace(",", String.Empty);
            }

            clearTranscript.Add(item.word);
        }


        foreach (string item in clearTranscript)
        {
            if (stepSelected.GetIncorrectWords() != null || stepSelected.GetIncorrectWords().Length != 0)
            {
                //Miramos si la palabra coincide con alguna del diccionario de palabras clave incorrectas
                foreach (string st in stepSelected.GetIncorrectWords())
                {
                    if (item.Equals(st, StringComparison.CurrentCultureIgnoreCase))
                    {
                        numberKeywordsSaidIncorrectly++;
                        Debug.Log("palabra incorrecta: " + st);
                    }
                }
            }

            //Miramos si la palabra coincide con alguna del diccionario de palabras clave correctas
            foreach (string st in stepSelected.GetCorrectWords())
            {
                if (item.Equals(st, StringComparison.CurrentCultureIgnoreCase))
                {
                    numberKeywordsSaidCorrectly++;
                }
            }

            //break;
        }

        Debug.Log("Numero de palabras incorrectas: " + numberKeywordsSaidIncorrectly);
        Debug.Log("Numero de palabras correctas: " + numberKeywordsSaidCorrectly);

        EvaluateTranscript(numberKeywordsSaidCorrectly, numberKeywordsSaidIncorrectly, stepSelected);
    }

    public void EvaluateTranscript(int numberKeywordsSaidCorrectly, int numberKeywordsSaidIncorrectly, Step currentStep)
    {

        if (numberKeywordsSaidIncorrectly >= currentStep.GetNIncorrectWords() || numberKeywordsSaidCorrectly < currentStep.GetNCorrectWords())
        {
            if (numberOfErrorVideoPlayed == 0)
            {
                videoManager.PlayVideo(steps[0].GetAssociatedErrorVideo(), true, false, false);
                numberOfErrorVideoPlayed++;
            }
            else
            {
                //Tenemos que finalizar la aplicación
                videoManager.PlayVideo(steps[0].GetAssociatedErrorVideo2(), true, false, true);
            }

        }
        else if (numberKeywordsSaidCorrectly >= currentStep.GetNCorrectWords())
        {

            //En este punto el usuario ha visto el video 1
            //luego se le ha mostrado el video 2
            //despues de mostrarle el video 2 el usuario ha hecho su pregunta
            //y la ha hecho bien al primer o al segundo intento (cosa que nos da igual)
            //por lo que ahora tenemos que mostrar el vídeo 10 luego el video 4 y, una vez que termine, cambiar de escena
            //ENTONCES LO QUE HACEMOS ES UNICO VIDEO QUE ENGLOBE EL VIDEO 10 Y EL 4.
            videoManager.PlayVideo("Video4_7_10", false, true, false);
            //videoManager.PlayVideo("Video5", false, true, false);
            etapaBienvenidaCompleted = true;
        }
    }


     public IEnumerator LoadSceneAsync(string scene)
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(scene);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    public void EndApplication()
    {
        demasiadosErrores.SetActive(true);
    }

    #region Debug
    [ContextMenu("Pasando paso")]
    
    void Debug01()
    {
        EvaluateTranscript(7,0, steps[0]);
    }

    [ContextMenu("Bool a true")]
    void Debug02()
    {
        etapaBienvenidaCompleted = true;
    }

    #endregion
}