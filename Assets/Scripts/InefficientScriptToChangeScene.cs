﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InefficientScriptToChangeScene : MonoBehaviour
{
    SceneManager sceneManager;
    // Start is called before the first frame update
    void Start()
    {
        sceneManager = FindObjectOfType<SceneManager>();
    }

    
    public void ChangeScene(string name)
    {
        StartCoroutine(sceneManager.LoadSceneAsync(name));
    }

    public void FinishApplication()
    {
        Application.Quit();
    }
}
