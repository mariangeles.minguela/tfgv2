﻿using UnityEngine;
using System.IO;

public static class CSVManager {
    private static string reportFileName = "report.csv";
    private static string reportSeparator = ",";
    private static string[] reportHeaders = new string[8] {
        "Nombre alumno",
        "Correo",
        "Apartado",
        "Errores cometidos",
        "Nivel de desempeño",
        "Problema en dependencia",
        "Tipo de apoyo personal",
        "Frecuencia de apoyo personal"
    };
    private static string timeStampHeader = "Fecha";

#region Interactions

    public static void AppendToReport(string[] strings) {
        VerifyDirectory();
        VerifyFile();
        using (StreamWriter sw = File.AppendText(GetFilePath())) {
            string finalString = "";
            for (int i = 0; i < strings.Length; i++) {
                finalString += strings[i];
                finalString += reportSeparator;
            }
            finalString += GetTimeStamp();
            sw.WriteLine(finalString);
        }
    }

    public static void CreateReport() {
        VerifyDirectory();
        using (StreamWriter sw = File.CreateText(GetFilePath())) {
            string finalString = "";
            for (int i = 0; i < reportHeaders.Length; i++) {
                finalString += reportHeaders[i];
                finalString += reportSeparator;
            }
            finalString += timeStampHeader;
            sw.WriteLine(finalString);
        }
    }
    public static void CreateReport(string fileName) {
        reportFileName = fileName + ".csv";
        VerifyDirectory();
        using (StreamWriter sw = File.CreateText(GetFilePath())) {
            string finalString = "";
            for (int i = 0; i < reportHeaders.Length; i++) {
                finalString += reportHeaders[i];
                finalString += reportSeparator;
            }
            finalString += timeStampHeader;
            sw.WriteLine(finalString);
        }
    }

#endregion


#region Operations

    static void VerifyDirectory() {
        string dir = GetDirectoryPath();
        if (!Directory.Exists(dir)) {
            Directory.CreateDirectory(dir);
        }
    }

    static void VerifyFile() {
        string file = GetFilePath();
        if (!File.Exists(file)) {
            CreateReport();
        }
    }

#endregion


#region Queries

    static string GetDirectoryPath() {
        return Application.persistentDataPath + "/";
    }

    static string GetFilePath() {
        return GetDirectoryPath() + "/" + reportFileName;
    }

    static string GetTimeStamp() {
        return System.DateTime.UtcNow.ToString();
    }

#endregion

}
