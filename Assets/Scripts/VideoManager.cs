﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{

    private TrainingManager trainingManager;
    private StartExperience startExperience;
    private VideoPlayer videoPlayer;

    public Action clipHasFinished;

    private bool once = true;
    int nVideosPlayed = 0;



    public void Awake()
    {
        videoPlayer = gameObject.GetComponent<VideoPlayer>();
        startExperience = FindObjectOfType<StartExperience>();
        trainingManager = FindObjectOfType<TrainingManager>();

        if (trainingManager != null)
        {
            trainingManager.PlayVideo += PlayVideo;
            trainingManager.PlayLastVideo += PlayLastVideo;
        }
    }

    public void PlayVideo(string vName, bool userHasToTalk, bool changeScene, bool demasiadosErrores)
    {
        videoPlayer.clip = Resources.Load<VideoClip>("Videos/" + vName);
        videoPlayer.Play();

        StartCoroutine(WaitUntilClipFinish(userHasToTalk, changeScene, demasiadosErrores));
    }

    public void PlayVideo(string videoName, GameObject questions, bool secondError)
    {
        videoPlayer.clip = Resources.Load<VideoClip>("Videos/" + videoName);
        StartCoroutine(WaitUntilClipFinish(questions, secondError));
    }

    public void PlayLastVideo(string videoName)
    {
        videoPlayer.clip = Resources.Load<VideoClip>("Videos/" + videoName);
        StartCoroutine(WaitUntilClipFinish());
    }

    IEnumerator WaitUntilClipFinish(GameObject questions, bool secondError)
    {
        videoPlayer.Play();
        yield return new WaitForSeconds((float)videoPlayer.length + 1f);

        if (secondError)
        {
            trainingManager.EndApplication();
        }
        else if (questions == null)
        {
            //Si entramos aqui significa que el usuario ha hecho la primera pregunta correctamente, se ha reproducido el vídeo correspondiente
            //y ahora tiene que hacer la segunda pregunta por lo tanto activamos el sistema de reconocimiento de voz
            //También puede ser que estemos aqui porque el usuario haya hecho la pregunta incorrectamente, por lo que tenemos que darle la oportunidad
            //de hacer de nuevo la pregunta por lo tanto activamos el sistema de reconocimiento de voz
            trainingManager.activitySelected.Invoke();
        }
        else
        {
            questions.SetActive(true);
        }
    }

    IEnumerator WaitUntilClipFinish()
    {
        videoPlayer.Play();
        yield return new WaitForSeconds((float)videoPlayer.length + 1f);
        //En este punto tenemos que enviar el email

        MailManager mailManager = FindObjectOfType<MailManager>();

        if (mailManager != null)
        {
            mailManager.SendMail();
        }

        TestFilesCreate testFilesCreate = FindObjectOfType<TestFilesCreate>();
        testFilesCreate.Upload();

        trainingManager.creditos.SetActive(true);
    }

    IEnumerator WaitUntilClipFinish(bool userHasToTalk, bool changeScene, bool secondError)
    {
        videoPlayer.Play();
        yield return new WaitForSeconds((float)videoPlayer.length + 1f);

        if (secondError)
        {
            startExperience.EndApplication();
        }
        else if (!userHasToTalk)
        {
            if (changeScene)
            {
                //StartCoroutine(startExperience.LoadSceneAsync("ProbandoVideo360"));
                trainingManager.activities.SetActive(true);
            }
            else
            {
                startExperience.ExecuteVoiceReconigtion();
            }
        }
        else
        {
            startExperience.ExecuteVoiceReconigtion();
        }
    }
}