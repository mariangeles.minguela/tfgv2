﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;


namespace FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples
{
	public class Custom_GCSR_Example : MonoBehaviour
	{
		private GCSpeechRecognition _speechRecognition;

		private VideoManager videoManager;
		private TrainingManager trainingManager;

		private StartExperience startExperience;

		private string _resultadoTexto;
		private Button _stopRecordButton,
					   _detectThresholdButton,
					   _cancelAllRequestsButton,
					   _recognizeButton,
					   _refreshMicrophonesButton;

		public Image _speechRecognitionState;

		private Text _resultText;

		private Toggle _voiceDetectionToggle,
					   _recognizeDirectlyToggle,
					   _longRunningRecognizeToggle;

		private Dropdown _microphoneDevicesDropdown;

		private InputField _contextPhrasesInputField,
						   _operationIdInputField;

		private Image _voiceLevelImage;
		private bool _isRecording = false;
		private bool _voiceDetection = true;

		private Color tempColor;

		private WordInfo[] temp;


        private void Start()
		{
			_speechRecognition = GCSpeechRecognition.Instance;
			_speechRecognition.RecognizeSuccessEvent += RecognizeSuccessEventHandler;
			_speechRecognition.RecognizeFailedEvent += RecognizeFailedEventHandler;
			_speechRecognition.LongRunningRecognizeSuccessEvent += LongRunningRecognizeSuccessEventHandler;
			_speechRecognition.LongRunningRecognizeFailedEvent += LongRunningRecognizeFailedEventHandler;
			_speechRecognition.GetOperationSuccessEvent += GetOperationSuccessEventHandler;
			_speechRecognition.GetOperationFailedEvent += GetOperationFailedEventHandler;
			_speechRecognition.ListOperationsSuccessEvent += ListOperationsSuccessEventHandler;
			_speechRecognition.ListOperationsFailedEvent += ListOperationsFailedEventHandler;

			_speechRecognition.FinishedRecordEvent += FinishedRecordEventHandler;
			_speechRecognition.StartedRecordEvent += StartedRecordEventHandler;
			_speechRecognition.RecordFailedEvent += RecordFailedEventHandler;

			_speechRecognition.BeginTalkigEvent += BeginTalkigEventHandler;
			_speechRecognition.EndTalkigEvent += EndTalkigEventHandler;

			_stopRecordButton = transform.Find("Canvas/Button_StopRecord").GetComponent<Button>();
			_detectThresholdButton = transform.Find("Canvas/Button_DetectThreshold").GetComponent<Button>();
			_cancelAllRequestsButton = transform.Find("Canvas/Button_CancelAllRequests").GetComponent<Button>();
			_recognizeButton = transform.Find("Canvas/Button_Recognize").GetComponent<Button>();
			_refreshMicrophonesButton = transform.Find("Canvas/Button_RefreshMics").GetComponent<Button>();

			//_speechRecognitionState = transform.Find("Canvas/Image_RecordState").GetComponent<Image>();

			_resultText = transform.Find("Canvas/Panel_ContentResult/Text_Result").GetComponent<Text>();

			_voiceDetectionToggle = transform.Find("Canvas/Toggle_DetectVoice").GetComponent<Toggle>();
			_recognizeDirectlyToggle = transform.Find("Canvas/Toggle_RecognizeDirectly").GetComponent<Toggle>();
			_longRunningRecognizeToggle = transform.Find("Canvas/Toggle_LongRunningRecognize").GetComponent<Toggle>();
			_microphoneDevicesDropdown = transform.Find("Canvas/Dropdown_MicrophoneDevices").GetComponent<Dropdown>();		

			_contextPhrasesInputField = transform.Find("Canvas/InputField_SpeechContext").GetComponent<InputField>();
			_operationIdInputField = transform.Find("Canvas/InputField_Operation").GetComponent<InputField>();

			_voiceLevelImage = transform.Find("Canvas/Panel_VoiceLevel/Image_Level").GetComponent<Image>();

			_stopRecordButton.onClick.AddListener(StopRecordButtonOnClickHandler);
			_detectThresholdButton.onClick.AddListener(DetectThresholdButtonOnClickHandler);
			_cancelAllRequestsButton.onClick.AddListener(CancelAllRequetsButtonOnClickHandler);
			_recognizeButton.onClick.AddListener(RecognizeButtonOnClickHandler);
			_refreshMicrophonesButton.onClick.AddListener(RefreshMicsButtonOnClickHandler);

			_microphoneDevicesDropdown.onValueChanged.AddListener(MicrophoneDevicesDropdownOnValueChangedEventHandler);

			_stopRecordButton.interactable = false;
			tempColor = _speechRecognitionState.color;
         	tempColor.a = 0f;
          	_speechRecognitionState.color = tempColor;

			//Buscamos al VideoManager para suscribirnos al evento de empezar a grabar.
			//De esta manera nos notificará cuándo podemos empezar a grabar al usuario
			videoManager = GameObject.FindObjectOfType<VideoManager>();
			trainingManager = GameObject.FindObjectOfType<TrainingManager>();
			startExperience = GameObject.FindObjectOfType<StartExperience>();

			if(videoManager != null)
			{
				videoManager.clipHasFinished += StartRecordButtonOnClickHandler;
			}

			if(trainingManager != null)
			{
				trainingManager.activitySelected += StartRecordButtonOnClickHandler;
			}

			RefreshMicsButtonOnClickHandler();
		}

		private void OnDestroy()
		{
			_speechRecognition.RecognizeSuccessEvent -= RecognizeSuccessEventHandler;
			_speechRecognition.RecognizeFailedEvent -= RecognizeFailedEventHandler;
			_speechRecognition.LongRunningRecognizeSuccessEvent -= LongRunningRecognizeSuccessEventHandler;
			_speechRecognition.LongRunningRecognizeFailedEvent -= LongRunningRecognizeFailedEventHandler;
			_speechRecognition.GetOperationSuccessEvent -= GetOperationSuccessEventHandler;
			_speechRecognition.GetOperationFailedEvent -= GetOperationFailedEventHandler;
			_speechRecognition.ListOperationsSuccessEvent -= ListOperationsSuccessEventHandler;
			_speechRecognition.ListOperationsFailedEvent -= ListOperationsFailedEventHandler;

			_speechRecognition.FinishedRecordEvent -= FinishedRecordEventHandler;
			_speechRecognition.StartedRecordEvent -= StartedRecordEventHandler;
			_speechRecognition.RecordFailedEvent -= RecordFailedEventHandler;

			_speechRecognition.EndTalkigEvent -= EndTalkigEventHandler;
		}

		private void Update()
		{
			if(_speechRecognition.IsRecording)
			{
				if(_speechRecognition.GetMaxFrame() > 0.01f)
				{
					//StopAutomatically();
				}

				if (_speechRecognition.GetMaxFrame() > 0)
				{
					float max = (float)_speechRecognition.configs[_speechRecognition.currentConfigIndex].voiceDetectionThreshold;
					float current = _speechRecognition.GetLastFrame() / max;

					if(current >= 1f)
					{
						_voiceLevelImage.fillAmount = Mathf.Lerp(_voiceLevelImage.fillAmount, Mathf.Clamp(current / 2f, 0, 1f), 30 * Time.deltaTime);
					}
					else
					{
						_voiceLevelImage.fillAmount = Mathf.Lerp(_voiceLevelImage.fillAmount, Mathf.Clamp(current / 2f, 0, 0.5f), 30 * Time.deltaTime);
					}
				}
			}
		}

		private void RefreshMicsButtonOnClickHandler()
		{
			_speechRecognition.RequestMicrophonePermission(null);

			_microphoneDevicesDropdown.ClearOptions();

			for (int i = 0; i < _speechRecognition.GetMicrophoneDevices().Length; i++)
			{
				_microphoneDevicesDropdown.options.Add(new Dropdown.OptionData(_speechRecognition.GetMicrophoneDevices()[i]));
			}

			//smart fix of dropdowns
			_microphoneDevicesDropdown.value = 1;
			_microphoneDevicesDropdown.value = 0;
		}

		private void MicrophoneDevicesDropdownOnValueChangedEventHandler(int value)
		{
			if (!_speechRecognition.HasConnectedMicrophoneDevices())
				return;
			_speechRecognition.SetMicrophoneDevice(_speechRecognition.GetMicrophoneDevices()[value]);
		}

		public void StartRecordButtonOnClickHandler()
		{
			//_detectThresholdButton.interactable = false;
			_resultText.text = string.Empty;
			_isRecording = true;

			_speechRecognition.StartRecord(_voiceDetection);
		}

		private void StopRecordButtonOnClickHandler()
		{
			_stopRecordButton.interactable = false;
			//_detectThresholdButton.interactable = true;

			Debug.Log("PARANDO LA GRABACION");

			_speechRecognition.StopRecord();
		}

		private void GetOperationButtonOnClickHandler()
		{
			if(string.IsNullOrEmpty(_operationIdInputField.text))
			{
				_resultText.text = "<color=red>Operatinon name is empty</color>";
				return;
			}

			_speechRecognition.GetOperation(_operationIdInputField.text);
		}

		private void GetListOperationsButtonOnClickHandler()
		{
			// some parameters could be seted
			_speechRecognition.GetListOperations();
		}

		private void DetectThresholdButtonOnClickHandler()
		{
			_speechRecognition.DetectThreshold();
		}

		private void CancelAllRequetsButtonOnClickHandler()
		{
			_speechRecognition.CancelAllRequests();
		}

		private void RecognizeButtonOnClickHandler()
		{
			if (_speechRecognition.LastRecordedClip == null)
			{
				_resultText.text = "<color=red>No Record found</color>";
				return;
			}

			FinishedRecordEventHandler(_speechRecognition.LastRecordedClip, _speechRecognition.LastRecordedRaw);
		}

		private void StartedRecordEventHandler()
		{
         	tempColor.a = 1f;
          	_speechRecognitionState.color = tempColor;
		}

		private void RecordFailedEventHandler()
		{
         	tempColor.a = 0f;
          	_speechRecognitionState.color = tempColor;
			_resultText.text = "<color=red>Start record Failed. Please check microphone device and try again.</color>";

			_stopRecordButton.interactable = false;
		}

		private void BeginTalkigEventHandler()
		{
			_resultText.text = "<color=blue>Talk Began.</color>";
			Debug.Log("<color=blue>Talk began.</color>");
		}

		private void EndTalkigEventHandler(AudioClip clip, float[] raw)
		{
			_resultText.text += "\n<color=blue>Talk Ended.</color>";
			Debug.Log("<color=blue>Talk end.</color>");
			//FinishedRecordEventHandler(clip, raw);
			if(_isRecording)
			{
				StopRecordButtonOnClickHandler();
			}
			
		}

		private void FinishedRecordEventHandler(AudioClip clip, float[] raw)
		{
			tempColor.a = 0f;
			_speechRecognitionState.color = tempColor;
			if (!_voiceDetection)
			{
         		tempColor.a = 0f;
          		_speechRecognitionState.color = tempColor;
			}

			if (clip == null || !_recognizeDirectlyToggle.isOn)
				return;

			RecognitionConfig config = RecognitionConfig.GetDefault();
			config.speechContexts = new SpeechContext[]
			{
				new SpeechContext()
				{
					phrases = _contextPhrasesInputField.text.Replace(" ", string.Empty).Split(',')
				}
			};
			config.audioChannelCount = clip.channels;
			// configure other parameters of the config if need

			GeneralRecognitionRequest recognitionRequest = new GeneralRecognitionRequest()
			{
				audio = new RecognitionAudioContent()
				{
					content = raw.ToBase64()
				},
				//audio = new RecognitionAudioUri() // for Google Cloud Storage object
				//{
				//	uri = "gs://bucketName/object_name"
				//},
				config = config
			};

			if (_longRunningRecognizeToggle.isOn)
			{
				_speechRecognition.LongRunningRecognize(recognitionRequest);
			}
			else
			{
				_speechRecognition.Recognize(recognitionRequest);
			}
		}

		private void GetOperationFailedEventHandler(string error)
		{
			_resultText.text = "Get Operation Failed: " + error;
		}

		private void ListOperationsFailedEventHandler(string error)
		{
			_resultText.text = "List Operations Failed: " + error;
		}

		private void RecognizeFailedEventHandler(string error)
        {
            _resultText.text = "Recognize Failed: " + error;
        }

		private void LongRunningRecognizeFailedEventHandler(string error)
		{
			_resultText.text = "Long Running Recognize Failed: " + error;
		}

		private void ListOperationsSuccessEventHandler(ListOperationsResponse operationsResponse)
		{
			_resultText.text = "List Operations Success.\n";

			if (operationsResponse.operations != null)
			{
				_resultText.text += "Operations:\n";

				foreach (var item in operationsResponse.operations)
				{
					_resultText.text += "name: " + item.name + "; done: " + item.done + "\n";
				}
			}
		}

		private void GetOperationSuccessEventHandler(Operation operation)
		{
			_resultText.text = "Get Operation Success.\n";
			_resultText.text += "name: " + operation.name + "; done: " + operation.done;

			if(operation.done && (operation.error == null || string.IsNullOrEmpty(operation.error.message)))
			{
				//InsertRecognitionResponseInfo(operation.response);
			}		
		}

		private void RecognizeSuccessEventHandler(RecognitionResponse recognitionResponse)
        {
			_resultText.text = "Recognize Success.";
			InsertRecognitionResponseInfo(recognitionResponse);
        }

        private void LongRunningRecognizeSuccessEventHandler(Operation operation)
        {
			if (operation.error != null || !string.IsNullOrEmpty(operation.error.message))
				return;

			_resultText.text = "Long Running Recognize Success.\n Operation name: " + operation.name;


			if (operation != null && operation.response != null && operation.response.results.Length > 0)
            {
                _resultText.text = "Long Running Recognize Success.";
				_resultText.text += "\n" + operation.response.results[0].alternatives[0].transcript;

				string other = "\nDetected alternatives:\n";

                foreach (var result in operation.response.results)
                {
                    foreach (var alternative in result.alternatives)
                    {
						if (operation.response.results[0].alternatives[0] != alternative)
						{
							other += alternative.transcript + ", ";
						}
                    }
                }

                _resultText.text += other;
            }
            else
            {
                _resultText.text = "Long Running Recognize Success. Words not detected.";
            }
        }

		private void InsertRecognitionResponseInfo(RecognitionResponse recognitionResponse)
		{
			if (recognitionResponse == null || recognitionResponse.results.Length == 0)
			{
				_resultText.text = "\nWords not detected.";
				Debug.Log("Empezando el sistema de nuevo");
				StartRecordButtonOnClickHandler();
				return;
			}

			_resultText.text += "\n" + recognitionResponse.results[0].alternatives[0].transcript;
			_resultadoTexto = recognitionResponse.results[0].alternatives[0].transcript;

			temp = recognitionResponse.results[0].alternatives[0].words;

			Debug.Log(JsonConvert.SerializeObject(recognitionResponse));

			if(startExperience.etapaBienvenidaCompleted)
			{
				trainingManager.ProcessTranscript(temp);
			}else
			{
				startExperience.ProcessTranscript(temp);
			}

			string other = "\nDetected alternatives: ";

			foreach (var result in recognitionResponse.results)
			{
				foreach (var alternative in result.alternatives)
				{
					if (recognitionResponse.results[0].alternatives[0] != alternative)
					{
						other += alternative.transcript + ", ";
					}
				}
			}

			_resultText.text += other;
		}
	}
}