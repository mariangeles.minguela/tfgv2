﻿using System.Collections.Generic;
using UnityEngine;
using UnityGoogleDrive;

public class TestAboutGet : MonoBehaviour
{
    private GoogleDriveAbout.GetRequest request;
    private GoogleDriveSettings settings;

    void Awake ()
    {
        settings = GoogleDriveSettings.LoadFromResources();
    }

    private void Start ()
    {
        UpdateInfo();
    }

    private void UpdateInfo ()
    {
        AuthController.CancelAuth();

        request = GoogleDriveAbout.Get();
        request.Fields = new List<string> { "user", "storageQuota" };
        request.Send();
    }
}
