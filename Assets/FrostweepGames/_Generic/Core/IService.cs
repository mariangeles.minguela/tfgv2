﻿namespace FrostweepGames.Plugins.Core
{
    public interface IService
    {
        void Init();
        void Update();
        //void UpdateService();
        void Dispose();
    }
}